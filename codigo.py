#Codigo adaptado de https://machinelearningmastery.com/machine-learning-in-python-step-by-step/
#Rode o dependencies.py primeiro para verificar dependencias.

# Load libraries
import pandas
from pandas.tools.plotting import scatter_matrix
import matplotlib.pyplot as plt
import numpy as np
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import export_graphviz
import graphviz 
import itertools
from sklearn import tree
from sklearn.model_selection import cross_val_predict
pandas.set_option('display.max_columns', None)

def plot_confusion_matrix(cm, classes,
                          title='Confusion matrix',
                          cmap=plt.cm.OrRd):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title, fontsize=20)
    plt.clim(0,1)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, fontsize=16)
    plt.yticks(tick_marks, classes, fontsize=16, rotation=90)
    fmt = '.2f'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt), fontsize=24,
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    #plt.tight_layout()
    plt.ylabel('Amostras', fontsize=16)
    plt.xlabel('Predicoes', fontsize=16)
    plt.tight_layout()


# Load dataset
#Da para usar o url direto. Mas por conveniencia, baixei.
#url = "https://archive.ics.uci.edu/ml/machine-learning-databases/spambase/spambase.data"
file="spambase.csv"
att_names = ['word_freq_make','word_freq_address','word_freq_all','word_freq_3d','word_freq_our','word_freq_over',
			'word_freq_remove','word_freq_internet','word_freq_order','word_freq_mail','word_freq_receive',
			'word_freq_will','word_freq_people','word_freq_report','word_freq_addresses','word_freq_free',
			'word_freq_business','word_freq_email','word_freq_you','word_freq_credit','word_freq_your',
			'word_freq_font','word_freq_000','word_freq_money','word_freq_hp','word_freq_hpl','ord_freq_george',
			'word_freq_650','word_freq_lab','word_freq_labs','word_freq_telnet','word_freq_857','word_freq_data',
			'word_freq_415','word_freq_85','word_freq_technology','word_freq_1999','word_freq_parts',
			'word_freq_pm','word_freq_direct','word_freq_cs','word_freq_meeting','word_freq_original',
			'word_freq_project','word_freq_re','word_freq_edu','word_freq_table','word_freq_conference',
			'char_freq_pontovirgula','char_freq_parentesis','char_freq_colchete','char_freq_exclamacao',
			'char_freq_cifrao','char_freq_hashtag','capital_run_length_average','capital_run_length_longest',
			'capital_run_length_total','classes']
dataset = pandas.read_csv(file, names=att_names)
target_names = dataset.classes.unique()[::-1]
print target_names
# Quantidade de amostras, quantidade de atributos
print(dataset.shape)

# Se quiser mostrar os X primeiros.
#print(dataset.head(2))

# Mostra dados dos atributos do dataset inteiro
#a = dataset.describe(percentiles=[])
#print(a)

# Distribuição das classes
print(dataset.groupby('classes').size())

# Divisão do dataset
array = dataset.values
X = array[:,0:57]
Y = array[:,57]
#Holdout 75/25
validation_size = 0.25
X_train, X_validation, Y_train, Y_validation = model_selection.train_test_split(X, Y, test_size=validation_size)

# Spot Check Algorithms
models = []
#Cart com balanceamento de carga
models.append(('CART', DecisionTreeClassifier(class_weight='balanced')))
#KNN considerando a distancias dos vizinhos
models.append(('KNN', KNeighborsClassifier(n_neighbors=3, weights='distance')))
# evaluate each model in turn
names = []

for name, model in models:
	print '_________________________%s____________________________' % (name)
	##############  Hold out  #######################
	model.fit(X_train, Y_train)
	predictions = model.predict(X_validation)

	# Matriz de confusao
	cnf_matrix = confusion_matrix(Y_validation, predictions)
	# Normalizador do matriz
	cm = cnf_matrix.astype('float') / cnf_matrix.sum(axis=1)[:, np.newaxis]
	np.set_printoptions(precision=2)
	# Plot da matriz
	plt.figure()
	plot_confusion_matrix(cm, classes=target_names, title='Matriz de Confusao Normalizada')
	plt.show()
	
	#Resultados
	print("Acuracia")
	print '%.2f' % accuracy_score(Y_validation, predictions)
	
	print("Matriz de Confusao")
	print(confusion_matrix(Y_validation, predictions))

	print("Matriz de Confusao Normalizada")
	print(cm)

	print("TP")
	print '%.2f' % cm[0,0]

	print("TN")
	print '%.2f' % cm[1,1]

	print("FP")
	print '%.2f' % cm[1,0]

	print("FN")
	print '%.2f' % cm[0,1]

	#Geração da árvore de decisão. Necessário a ferramenta graphviz para visualizar a arvore a partir do .dot
	#Alterar max_depth (ou remover) para poder ver a arvore completa.
	if 'CART' in name:
		export_graphviz(model, out_file="CART.dot",	feature_names=att_names[:57],class_names=target_names, 
			rounded=True, filled=True, proportion=False, label='root', max_depth=3, impurity=False, precision=1, rotate=True)